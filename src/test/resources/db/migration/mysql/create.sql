CREATE TABLE univers (
     id             BIGINT(20) NOT NULL AUTO_INCREMENT,
     version        INTEGER,
     create_by      VARCHAR(255) NOT NULL,
     create_date    DATE NOT NULL,
     update_by      VARCHAR(255),
     update_date    TIMESTAMP,
     status         VARCHAR(20),
     route_phase    VARCHAR(255),
     peso           NUMERIC(19,2) NOT NULL,
     PRIMARY KEY (id)
);

CREATE TABLE person (
    id              BIGINT(20) NOT NULL AUTO_INCREMENT,
    version         INTEGER,
    create_by       VARCHAR(255) NOT NULL,
    create_date     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    update_by       VARCHAR(255) NOT NULL,
    update_date     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    name            VARCHAR(255) NOT NULL,
    surname         VARCHAR(255) NOT NULL,
    email_address   VARCHAR(255),
    phone_number    VARCHAR(20),
    sec_user_id     BIGINT NOT NULL,
    univers_id      BIGINT NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE person ADD CONSTRAINT fk_p_u FOREIGN KEY (univers_id) REFERENCES univers(id);

