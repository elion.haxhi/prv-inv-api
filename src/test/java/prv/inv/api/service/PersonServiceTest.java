package prv.inv.api.service;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import prv.inv.api.entity.Person;
import prv.inv.api.entity.Univers;
import prv.inv.api.exception.UniversException;
import prv.inv.api.UnitTests;
import prv.inv.api.model.PersonResource;
import prv.inv.api.model.PersonResp;
import prv.inv.api.repository.PersonRepository;

import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Optional;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTests.class)
@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    public static final String TOKEN_JWT = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0." +
            "e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
            ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
    public static final String OK = "La chiamata è andata a buon fine";

    @InjectMocks
    PersonService personServiceSUT;

    @Mock
    AuthorizationService authorizationService;

    @Mock
    PersonRepository personRepository;

    @Test
    public void shouldThrowUniversExceptionOnGetPersonByUniversId()throws NoSuchAlgorithmException {

        catchException(personServiceSUT).retrievePersonListResourceByUniversId(TOKEN_JWT,1L);

        assertTrue(caughtException() instanceof UniversException);
        assertEquals("Token credential are not present on db", ((UniversException) caughtException()).getMessage());

    }

    @Test
    public void shouldThrowAnUniversExceptionForInvalidToken() throws NoSuchAlgorithmException{
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(false);

        catchException(personServiceSUT).retrieveAPersonResourceById(TOKEN_JWT, 1);
        assertTrue(caughtException() instanceof UniversException);
    }

    @Test
    public void shouldReturnAPersonRespGivenAPerson(){
        Person person = mockPerson();
        PersonResp personResp = PersonResp.builder()
                .toPersonResp(person)
                .build();
        assertEqualsClasses(person, personResp);
    }


    @Test
    public void shouldReturnAPersonResourceOnGetPersonById()throws NoSuchAlgorithmException{
        Univers univers = mock(Univers.class);
        Person person = mockPerson();

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));

        PersonResource personResource = personServiceSUT.retrieveAPersonResourceById(TOKEN_JWT, 1);

        assertEquals("200",personResource.getMessageCode());
        assertEquals(OK,personResource.getMessage());
        assertEquals("elion",personResource.getPersonResp().getName());
    }

    private void assertEqualsClasses(Person person, PersonResp personResp) {
        assertEquals(person.getCreateDate(), personResp.getCreateDate());
        assertEquals(person.getCreateBy(), personResp.getCreateBy());
        assertEquals(person.getName(), personResp.getName());
        assertEquals(person.getSurname(), personResp.getSurname());
        assertEquals(person.getEmail(), personResp.getEmail());
        assertEquals(person.getPhone(), personResp.getPhone());
        assertNull(personResp.getLastModifiedBy());
        assertNull(personResp.getLastModifiedDate());
    }

    private Person mockPerson() {
        Person person = mock(Person.class);
        when(person.getCreateBy()).thenReturn("elion");
        when(person.getCreateDate()).thenReturn(Instant.now());
        when(person.getId()).thenReturn(1L);
        when(person.getName()).thenReturn("elion");
        when(person.getSurname()).thenReturn("haxhi");
        when(person.getEmail()).thenReturn("a.b@gmail.com");
        when(person.getPhone()).thenReturn("3200000000");
        return person;
    }

}