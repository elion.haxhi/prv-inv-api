package prv.inv.api.service;

import com.querydsl.core.types.Predicate;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import prv.inv.api.UnitTests;
import prv.inv.api.entity.Person;
import prv.inv.api.entity.Univers;
import prv.inv.api.enums.UniversStatus;
import prv.inv.api.exception.UniversException;
import prv.inv.api.model.*;
import prv.inv.api.repository.PersonRepository;
import prv.inv.api.repository.UniversRepository;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static prv.inv.api.service.AuthorizationServiceTest.USER;

@Category(UnitTests.class)
@RunWith(MockitoJUnitRunner.class)
public class UniversServiceWithTokenJWTTest {

    public static final String TOKEN_JWT = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0." +
            "e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
            ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
    public static final String QUERY = "";
    public static final String OK = "La chiamata e andata a buon fine";

    @Mock
    AuthorizationService authorizationService;

    @Mock
    UniversRepository universRepository;

    @Mock
    PersonRepository personRepository;

    @InjectMocks
    UniversService universService;

    @InjectMocks
    PersonService personService;

    @Test
    public void shouldThrowAnUniversExceptionForWrongCredentials()throws NoSuchAlgorithmException {
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(false);

        catchException(universService).queryUniverses(TOKEN_JWT, QUERY);
        assertTrue(caughtException() instanceof UniversException);
        assertEquals("Token credential are not present on db", ((UniversException) caughtException()).getMessage());
    }

    @Test
    public void shouldReturnUniversResourceListForATokenAndAQuery()throws NoSuchAlgorithmException{

        Univers u1 = mockUnivers();
        Univers u2 = mockUnivers();
        List<Univers> universes = Arrays.asList(u1, u2);

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
        when(universRepository.findAll(any(Predicate.class))).thenReturn(universes);

        UniversListResource url = universService.queryUniverses(TOKEN_JWT, QUERY);

        assertNotNull(url);
        assertEquals(2, url.getUnivers().size());
        assertEquals(ResponseStatus.SUCCESS, url.getStatus());
        assertEquals("200", url.getMessageCode());
        assertEquals("The call was executed correctly.", url.getMessage());

    }

    @Test
    public void shouldReturnUniversResourceListForAToken()throws NoSuchAlgorithmException{

        Univers u1 = mockUnivers();
        Univers u2 = mockUnivers();
        List<Univers> universes = Arrays.asList(u1, u2);
        

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
        when(universRepository.findAll()).thenReturn(universes);

        UniversListResource url = universService.getUniverses(TOKEN_JWT);

        assertNotNull(url);
        assertEquals(2, url.getUnivers().size());
        assertEquals(ResponseStatus.SUCCESS, url.getStatus());
        assertEquals("200", url.getMessageCode());
        assertEquals("The call was executed correctly.", url.getMessage());

    }

    @Test
    public void shouldThrowAnUniversExceptionGivenAToken() throws NoSuchAlgorithmException{
        UniversReq universReq = mockUniversReq();

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(false);

        catchException(universService).getUniverses(TOKEN_JWT);

        assertTrue(caughtException() instanceof UniversException);
        assertEquals("Token credential are not present on db", ((UniversException) caughtException()).getMessage());

    }

    @Test
    public void shouldSaveAUniversGivenAnUniversReqAndToken() throws NoSuchAlgorithmException{

        UniversReq universReq = mockUniversReq();
        Univers univers =Univers.builder().build().wrapToUnivers(universReq,USER);

        when(universRepository.save(any(Univers.class))).thenReturn(univers);
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);

        UniversResource ur = universService.createUnivers(TOKEN_JWT, universReq);

        assertEquals(ResponseStatus.SUCCESS, ur.getStatus());
        assertEquals(BigDecimal.ONE,ur.getUniversResp().getPeso());
        assertEquals("A",ur.getUniversResp().getRoutePhase());
        assertEquals(UniversStatus.STARTED,ur.getUniversResp().getStatus());

    }

    @Test
    public void shouldThrowAnUniversExceptionGivenAnUniversReqAndToken() throws NoSuchAlgorithmException{
        UniversReq universReq = mockUniversReq();

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(false);

        catchException(universService).createUnivers(TOKEN_JWT,universReq);

        assertTrue(caughtException() instanceof UniversException);
        assertEquals("Token credential are not present on db", ((UniversException) caughtException()).getMessage());

    }

    private UniversReq mockUniversReq(){
        UniversReq universReqStub = mock(UniversReq.class);

        when(universReqStub.getPeso()).thenReturn(BigDecimal.ONE);
        when(universReqStub.getRoutePhase()).thenReturn("A");
        when(universReqStub.getStatus()).thenReturn(UniversStatus.STARTED);

        return universReqStub;
    }

    @Test
    public void shouldUpdateAnUniversGivenATokenAnIdAndUniversReq() throws NoSuchAlgorithmException{
        UniversReq universReq = UniversReq.of(UniversStatus.STARTED, "A", BigDecimal.ONE);
        Univers univers = Univers.builder()
                                 .peso(BigDecimal.TEN)
                                 .routePhase("B")
                                 .status(UniversStatus.COMPLETED)
                                 .build();

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
        when(universRepository.getOne(anyLong())).thenReturn(univers);
        when(universRepository.save(any(Univers.class))).thenReturn(univers);

        UniversResource result = universService.updateUnivers(TOKEN_JWT, universReq, anyLong());

        assertEquals(BigDecimal.ONE, result.getUniversResp().getPeso());
        assertEquals("A", result.getUniversResp().getRoutePhase());
        assertEquals(UniversStatus.STARTED, result.getUniversResp().getStatus());
        assertEquals("200", result.getMessageCode());
        assertEquals("The call update is performed correctly", result.getMessage());
        assertEquals(ResponseStatus.SUCCESS, result.getStatus());


    }

    @Test
    public void shouldThrowAnUniversExceptionOnUpdateUnivers() throws NoSuchAlgorithmException{
        UniversReq universReq = mockUniversReq();

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(false);

        catchException(universService).updateUnivers(TOKEN_JWT,universReq,1L);

        assertTrue(caughtException() instanceof UniversException);
        assertEquals("Token credential are not present on db", ((UniversException) caughtException()).getMessage());

    }

    @Test
    public void shouldThrowAnUniversExceptionOnDeleteUnivers()throws NoSuchAlgorithmException {

        catchException(universService).deleteUniverse(TOKEN_JWT,1L);

        assertTrue(caughtException() instanceof UniversException);
        assertEquals("Token credential are not present on db", ((UniversException) caughtException()).getMessage());
    }

    @Test
    public void shouldReturnAnuniversResourceODeleteUnivers() throws NoSuchAlgorithmException{

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);

        UniversResource result = universService.deleteUniverse(TOKEN_JWT,1L);

        assertEquals("200", result.getMessageCode());
        assertEquals("The call was executed correctly.",result.getMessage());
        assertEquals(ResponseStatus.SUCCESS, result.getStatus());
        assertNull(result.getUniversResp());
    }



    @Test
    public void shouldReturnEmptyUniversResourceListForATokenAndAQuery()throws NoSuchAlgorithmException{

        List<Univers> universes = Arrays.asList();

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
        when(universRepository.findAll(any(Predicate.class))).thenReturn(universes);

        UniversListResource url = universService.queryUniverses(TOKEN_JWT, QUERY);

        assertNotNull(url);
        assertTrue(url.getUnivers().isEmpty());
        assertEquals(ResponseStatus.SUCCESS, url.getStatus());
        assertEquals("200", url.getMessageCode());
        assertEquals("The call was executed correctly.", url.getMessage());

    }


    private Univers mockUnivers() {
        Univers univers = mock(Univers.class);
        when(univers.getId()).thenReturn(1L);
        when(univers.getVersion()).thenReturn(0);
        when(univers.getCreateBy()).thenReturn("elion");
        when(univers.getCreatedDate()).thenReturn(LocalDate.now());
        when(univers.getStatus()).thenReturn(UniversStatus.COMPLETED);
        when(univers.getRoutePhase()).thenReturn("A");
        when(univers.getPeso()).thenReturn(BigDecimal.ONE);
        when(univers.getPersons()).thenReturn(Arrays.asList());
        return univers;
    }

    @Test
    public void shouldReturnAPersonListResourceForAnUnivers()throws NoSuchAlgorithmException{

        Univers univers = mockUnivers();

        Person person = mockPerson(univers);

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
        when(personRepository.trovaTutteLePersonPerUnivers(anyLong())).thenReturn(Arrays.asList(person));

        PersonListResource personListResource = personService.retrievePersonListResourceByUniversId(TOKEN_JWT,1);

        assertEquals(1,personListResource.getPerson().size());
        assertEquals(OK, personListResource.getMessage());
        assertEquals("200", personListResource.getMessageCode());
        assertEquals(ResponseStatus.SUCCESS, personListResource.getStatus());
    }

    @Test
    public void shouldReturnAPersonListResourceForAnUniversWithNoPersonInIt()throws NoSuchAlgorithmException{
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
        when(personRepository.trovaTutteLePersonPerUnivers(anyLong())).thenReturn(Mockito. <Person>anyList());

        PersonListResource personListResource = personService.retrievePersonListResourceByUniversId(TOKEN_JWT,1);

        assertEquals(0,personListResource.getPerson().size());
        assertEquals(OK, personListResource.getMessage());
        assertEquals("200", personListResource.getMessageCode());
        assertEquals(ResponseStatus.SUCCESS, personListResource.getStatus());
    }

    @Test
    public void shouldThrowAnUniversExceptionForInvalidToken() throws NoSuchAlgorithmException{
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(false);

        assertThatExceptionOfType(UniversException.class).isThrownBy(() ->
                personService.retrievePersonListResourceByUniversId(TOKEN_JWT, 1));
    }

    private Person mockPerson(Univers univers) {
        Person person = mock(Person.class);
        when(person.getCreateBy()).thenReturn("elion");
        when(person.getCreateDate()).thenReturn(Instant.now());
        when(person.getId()).thenReturn(1L);
        when(person.getName()).thenReturn("elion");
        when(person.getSurname()).thenReturn("haxhi");
        when(person.getEmail()).thenReturn("a.b@gmail.com");
        when(person.getPhone()).thenReturn("3200000000");
        return person;
    }

}