package prv.inv.api.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import prv.inv.api.Integration;
import prv.inv.api.entity.Person;
import prv.inv.api.entity.Univers;
import prv.inv.api.exception.UniversException;
import prv.inv.api.model.PersonResource;
import prv.inv.api.repository.PersonRepository;

import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Optional;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(Integration.class)
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonServiceIntegrationTest {

    public static final String TOKEN_JWT = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0." +
            "e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
            ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
    public static final String TOKEN_JWT_INVALID = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0" +
            ".e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWFkbWlufQ" +
            ".PIaq4IEwMSiXoa59deHuWLZ58MlTV3NIjZzsSO4jUeQ";
    public static final String OK = "La chiamata è andata a buon fine";

    @Autowired
    PersonService personService;

    @Test
    public void shouldThrowUniversExceptionOnGetPersonByUniversId()throws NoSuchAlgorithmException {

        catchException(personService).retrievePersonListResourceByUniversId(TOKEN_JWT_INVALID,1L);

        assertTrue(caughtException() instanceof UniversException);
        assertEquals("Token credential are not present on db", ((UniversException) caughtException()).getMessage());

    }

    @Test
    public void shouldThrowAnUniversExceptionForInvalidToken() throws NoSuchAlgorithmException{

        catchException(personService).retrieveAPersonResourceById(TOKEN_JWT_INVALID, 1);
        assertTrue(caughtException() instanceof UniversException);
    }

    @Test
    public void shouldReturnAPersonResourceOnGetPersonById()throws NoSuchAlgorithmException{
        PersonResource personResource = personService.retrieveAPersonResourceById(TOKEN_JWT, 1L);

        assertEquals("200",personResource.getMessageCode());
        assertEquals(OK,personResource.getMessage());
        assertEquals("Elion",personResource.getPersonResp().getName());
    }
}