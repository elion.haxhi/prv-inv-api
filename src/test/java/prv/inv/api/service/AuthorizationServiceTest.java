package prv.inv.api.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import prv.inv.api.UnitTests;
import prv.inv.api.exception.PayloadException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.*;
@Category(UnitTests.class)
@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class AuthorizationServiceTest {

    public static final String USER = "elion";
    public static final String PASSWORD = "admin";
    public static final String ENCODED_PAYLOAD = "e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ";
    public static final String ENCODED_HEADER = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0";
    public static final String ENCODED_SIGNATURE = "JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
    public static final String TOKEN_JWT = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0.e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ.JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";

    @InjectMocks
    AuthorizationService authorizationService;

    @Test
    public void givenPayload_returnUserElionAndPasswordAdmin(){

        Map<String, String> map = authorizationService.createPayload();
        assertEquals("Assert User Elion not correct","elion", map.get("user"));
        assertEquals("Assert Password Admin not correct","admin", map.get("password"));

    }

    @Test
    public void givenAPayloadMap_returnEncodeOfPayloadMap(){

        String expected = "e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ";
        Map<String, String> map = authorizationService.createPayload();
        String actual = authorizationService.encode(map);
        assertNotNull(actual);
        assertEquals(expected, actual);
        log.info("Value: {}",actual);
    }

    @Test
    public void givenAHeaderMap_returnEncodeOfHeaderMap(){

        String expected = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0";
        Map<String, String> map = authorizationService.createHeader();
        String actual = authorizationService.encode(map);
        assertNotNull(actual);
        assertEquals(expected, actual);
        log.info("Value: {}",actual);
    }

    @Test
    public void givenAnEncodedHeaderAndAnEncodedPayload_returnASignature(){

        String actual = authorizationService.hmacSha256(ENCODED_HEADER,ENCODED_PAYLOAD);
        assertNotNull(actual);
        assertEquals(ENCODED_SIGNATURE,actual);
        log.info("Value Signature {}",actual);
    }

    @Test
    public void shouldReturnATokenJWT(){
        String actual = authorizationService.generateToken();
        assertNotNull(actual);
        assertEquals(TOKEN_JWT, actual);
        log.info("Value token {} " , actual);
    }

    @Test
    public void givenAnEncodedPayload_returnAPlainPayload(){
        Map<String,String> result = authorizationService.decode(ENCODED_PAYLOAD);
        assertEquals(USER, result.get("user"));
        assertEquals(PASSWORD, result.get("password"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenAWrongToken_throwAnIlegalArgumentException(){
        String wrongToken = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ.JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
        try{
            authorizationService.decodeToken(wrongToken);
        } catch (NoSuchAlgorithmException e){
            fail("Wrong Token " + e.getMessage());
        }
    }

    @Test(expected = NoSuchAlgorithmException.class)
    public void givenAWrongEncodedHeaderOnToken_throwAnNoSuchAlgorithmException() throws NoSuchAlgorithmException{
        String wrongEncodedHeaderOnToken = "e3R5cD1KV1QsIGFsZz1IUzI1Nn1" +
                ".e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
                ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
            authorizationService.decodeToken(wrongEncodedHeaderOnToken);
    }

    @Test
    public void givenAToken_returnaPayloadWithUserAndPassword() throws NoSuchAlgorithmException{
        String token = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0" +
                ".e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
                ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
        Map<String,String> payload = authorizationService.decodeToken(token);
        assertEquals(USER, payload.get("user"));
        assertEquals(PASSWORD, payload.get("password"));
    }

    @Test(expected = PayloadException.class)
    public void givenATokenWithWrongPayload_throwAPayloadException()throws NoSuchAlgorithmException{
        String tokenWithWrongPayload = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0" +
                ".e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufK" +
                ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
        Map<String,String> payload = authorizationService.decodeToken(tokenWithWrongPayload);
    }

    @Test
    public void givenATokenWithWrongPaylaod_shouldThrowaPayloadException()throws NoSuchAlgorithmException{
        String tokenWithWrongPayload = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0" +
                ".e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufK" +
                ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";

            catchException(authorizationService).isAuthenticated(tokenWithWrongPayload);
            assertTrue(caughtException() instanceof PayloadException);
    }

    @Test
    public void givenAToken_returnAnAuthenticationFlag() throws NoSuchAlgorithmException{
        assertTrue("user or password not correct", authorizationService.isAuthenticated(TOKEN_JWT));
    }


    @Test
    public void retrieveEncodedHeaderHS256ForALGAndJWTForTYP(){
       Map<String, String> headerMap = authorizationService.createHeader();
       assertEquals("HS256", headerMap.get("alg"));
       assertEquals("JWT", headerMap.get("typ"));
    }

}