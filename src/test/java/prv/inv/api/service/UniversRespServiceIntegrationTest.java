package prv.inv.api.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import prv.inv.api.Integration;
import prv.inv.api.entity.Univers;
import prv.inv.api.enums.UniversStatus;
import prv.inv.api.model.UniversReq;
import prv.inv.api.model.UniversResource;
import prv.inv.api.repository.UniversRepository;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Category(Integration.class)
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UniversRespServiceIntegrationTest{

    public static final String ROUTE_PHASE = "A";
    public static final String TOKEN_JWT = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0." +
            "e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
            ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";

    @Autowired
    UniversService universService;

    @Autowired
    UniversRepository universRepository;

    @Test
    public void findById(){
        Optional<Univers> optional = universRepository.findById(1L);
        Univers u = optional.orElseThrow(RuntimeException::new);
        assertNotNull(u);
    }

    @Test
    public void shouldSaveAUnivers() throws NoSuchAlgorithmException{
        UniversReq universReq = UniversReq.of(UniversStatus.PENDING,"A", BigDecimal.ONE);
        UniversResource universResource = universService.createUnivers(TOKEN_JWT,universReq);

        assertEquals(UniversStatus.PENDING, universResource.getUniversResp().getStatus());
        assertEquals("A", universResource.getUniversResp().getRoutePhase());
        assertEquals(BigDecimal.ONE, universResource.getUniversResp().getPeso());
        assertEquals(LocalDate.now(), universResource.getUniversResp().getCreatedDate());
    }

}