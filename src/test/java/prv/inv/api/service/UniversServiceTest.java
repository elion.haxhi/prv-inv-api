//FIXME per creare metodo test vai su service , seleziona il nome classe è fai ctrl + shift + t
//FIXME per creare metodo su service seleziona il nomemetodo e fai alt + enter e ti genera il metodo vuoto su service
package prv.inv.api.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import prv.inv.api.UnitTests;
import prv.inv.api.entity.Person;
import prv.inv.api.entity.Univers;
import prv.inv.api.enums.UniversStatus;
import prv.inv.api.exception.UniversException;
import prv.inv.api.model.*;
import prv.inv.api.repository.UniversRepository;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static prv.inv.api.util.PrvInvConstants.OK_CALL;
import static prv.inv.api.util.PrvInvConstants.TOKEN_VALID_BUT_USER_NOT_EXIST;


@Category(UnitTests.class)
@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class UniversServiceTest {

    public static final String ME = "personName";
    public static final String PERSON_NAME = "personName";
    public static final String PERSON_SURNAME = "personSurname";
    public static final String EMAIL = "email@gmail.com";
    public static final String PHONE = "3200000000";
    public static final String ROUTE_PHASE_A = "A";
    public static final String ROUTE_PHASE_B = "B";
    public static final String OK_MESSAGE_SAVE = "The call save is performed correctly";
    public static final String KO_MESSAGE_SAVE = "The call save is performed with error";
    public static final String TOKEN_JWT = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0.e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
            ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";

    @Mock
    UniversRepository universRepositoryJpa;

    @Mock
    AuthorizationService authorizationService;

    @InjectMocks
    UniversService universService;


    @Test
    public void shouldReturnAnUniversResourceWithSuccess() throws NoSuchAlgorithmException {
        Univers univers = mockUnivers();

        when(universRepositoryJpa.findById(1L)).thenReturn(Optional.of(univers));
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);

        UniversResource actual = universService.getUniverse(1L, TOKEN_JWT);

        assertEquals(1L, actual.getUniversResp().getId().longValue());
        assertEquals(ResponseStatus.SUCCESS, actual.getStatus());
        assertEquals("200", actual.getMessageCode());
        assertEquals(OK_CALL, actual.getMessage());
        assertNotNull(actual.getUniversResp());
    }

    @Test
    public void shouldThrowAnUniversExceptionForaNotAuthenticatedToken() throws NoSuchAlgorithmException {
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(false);

        catchException(universService).getUniverse(1L, TOKEN_JWT);
        assertTrue(caughtException() instanceof UniversException);
        assertEquals(TOKEN_VALID_BUT_USER_NOT_EXIST, caughtException().getMessage());
    }

    private Univers mockUnivers() {
        Univers univers = mock(Univers.class);
        when(univers.getId()).thenReturn(1L);
        when(univers.getVersion()).thenReturn(0);
        when(univers.getCreateBy()).thenReturn("elion");
        when(univers.getCreatedDate()).thenReturn(LocalDate.now());
        when(univers.getStatus()).thenReturn(UniversStatus.COMPLETED);
        when(univers.getRoutePhase()).thenReturn("A");
        when(univers.getPeso()).thenReturn(BigDecimal.ONE);
        when(univers.getPersons()).thenReturn(Arrays.asList());
        return univers;
    }

    @Test
    public void shouldReturnAnUniversResourceDuringTheUpdateOfUnivers() throws NoSuchAlgorithmException{
        UniversReq universReq = UniversReq.of(UniversStatus.SCHEDULING,ROUTE_PHASE_B,
                new BigDecimal(12345.01));

        mockGetOneUnivers();
        mockSaveUnivers(universReq);

        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);

        UniversResource actual = universService.updateUnivers(TOKEN_JWT,universReq, 1L);

        assertEqualOfUpdatedFields(actual);
    }

    @Test
    public void shouldReturnAnUniversResourceGivenAnEmptyUniversResp(){
        UniversResource actual = universService.generateUniversResourceWithError(KO_MESSAGE_SAVE);

        assertNull(actual.getUniversResp());
        assertEquals(ResponseStatus.ERROR, actual.getStatus());
        assertEquals("500", actual.getMessageCode());
        assertEquals(KO_MESSAGE_SAVE, actual.getMessage());
    }

    @Test
    public void shouldReturnAnuniversResourceGivenAnuniversResp(){
        List<Univers> one = buildAListOfUnivers(11111.01);
        UniversResp oneResp = universService.wrapToUniversRespList(one).get(0);
        UniversResource actual = universService.generateUniversResourceFromUniversResp(oneResp, OK_MESSAGE_SAVE);

        assertNotNull(actual.getUniversResp());
        assertEquals(ResponseStatus.SUCCESS, actual.getStatus());
        assertEquals("200", actual.getMessageCode());
        assertEquals(OK_MESSAGE_SAVE, actual.getMessage());

    }

    @Test
    public void shouldReturnAnUniversResourceListGivenAnEmptyListOfUniversResp(){
        List<Univers> universes = buildAnEmptyListOfUnivers();
        List<UniversResp> universResps = universService.wrapToUniversRespList(universes);
        UniversListResource universListResource = universService.createUniverseList(universResps);

        assertEqualWithAnEmptyListOfUniversResp(universListResource);
    }

    @Test
    public void shouldReturnAUniversREsourceListGivenAListOfUniversResp(){
        List<Univers> universes = buildAListOfUnivers(11111.01, 11111.02, 11111.03);
        List<UniversResp> universResps = universService.wrapToUniversRespList(universes);
        UniversListResource universListResource = universService.createUniverseList(universResps);

        assertEqualsWithAListOfUniversResp(universListResource);
    }

    @Test
    public void shouldReturnAnEmptyListOfUniversRespGivenAListOfUnivers(){
        List<Univers> universes = buildAnEmptyListOfUnivers();
        List<UniversResp> universResps = universService.wrapToUniversRespList(universes);

        assertEquals(0,universResps.size());
        assertTrue(universResps.isEmpty());
    }

    @Test
    public void shouldReturnAListOfUniversRespGivenaListOfUnivers(){
        List<Univers> universes = buildAListOfUnivers(11111.01, 11111.02, 11111.03);
        List<UniversResp> universResps = universService.wrapToUniversRespList(universes);

        assertEquals(3,universResps.size());
        assertEquals(universes.size(),universResps.size());
        assertEquals(new BigDecimal(11111.01).setScale(2,1),universResps.get(0).getPeso());
        assertEquals(new BigDecimal(11111.02).setScale(2,1),universResps.get(1).getPeso());
        assertEquals(new BigDecimal(11111.03).setScale(2,1),universResps.get(2).getPeso());
    }

    @Test
    public void shouldReturnAnEmptyUniversResource() throws NoSuchAlgorithmException{
        mockSettingsForUnivers(Optional.empty());
        UniversResource actual = universService.getUniverse(1L, TOKEN_JWT);

        assertEqualsEmptyOptionalUnivers(actual);
    }


    @Test
    public void shouldReturnAnUniversResourceGivenAnUniversId() throws NoSuchAlgorithmException{
        mockSettingsForUnivers(buildAnOptionalUnivers(11111.01));
        UniversResource actual = universService.getUniverse(1L, TOKEN_JWT);

        assertEqualsOptionalUnivers(actual);
    }

    @Test
    public void shouldReturnAnUniversResourceGivenAnEmptyOptionalUnivers(){
        Optional<Univers> oUnivers = Optional.empty();
        UniversResource actual = universService.generateResponse(oUnivers);

        assertEqualsEmptyOptionalUnivers(actual);
    }

    @Test
    public void shouldReturnAnUniversResourceGivenAnOptionalUnivers(){
        Optional<Univers> oUnivers = buildAnOptionalUnivers(11111.01);
        UniversResource actual = universService.generateResponse(oUnivers);

        assertEqualsOptionalUnivers(actual);
    }

    @Test
    public void shouldReturnAnUniversRespWithNoPersonGivenAnUniversWithNoPerson(){
        Univers univers = buildAnOptionalUnivers(11111.01).get();
        UniversResp universResp = universService.wrapToUniversResp(univers);

        assertEqualsUniversCommonField(univers, universResp);
        assertEquals(0,universResp.getPersons().size());
        assertTrue(universResp.getPersons().isEmpty());

    }

    @Test
    public void shouldReturnAnUniversWithTwoPersonGivenAnUniversWithTwoPerson(){
        Univers univers = buildAnOptionalUnivers(buildAListOfPersons("p1","p2"), 11111.01).get();
        UniversResp universResp = universService.wrapToUniversResp(univers);

        assertEqualsUniversCommonField(univers, universResp);
        assertEquals(2,universResp.getPersons().size());
        assertEquals(univers.getPersons().size(), universResp.getPersons().size());

    }

    @Test
    public void shouldReturnAListOfPersonRespGivenAListOfPerson(){
        List<Person> expected = buildAListOfPersons();
        List<PersonResp> actual = universService.wrapToPersonResp(expected);
        assertEquals(expected.size(),actual.size());
    }

    @Test
    public void shouldReturnAnEmptyListOfPersonRespGivenAnEmptyListOfPerson(){
        List<Person> expected = new ArrayList<>();
        List<PersonResp> actual = universService.wrapToPersonResp(expected);
        assertEquals(expected.size(),actual.size());
    }

    private void mockSettingsForUnivers(Optional<Univers> empty) throws NoSuchAlgorithmException {
        Optional<Univers> oEmptyUnivers = empty;
        when(universRepositoryJpa.findById(anyLong())).thenReturn(oEmptyUnivers);
        when(authorizationService.isAuthenticated(TOKEN_JWT)).thenReturn(true);
    }

    private void assertEqualsWithAListOfUniversResp(UniversListResource universListResource) {
        assertFalse(universListResource.getUnivers().isEmpty());
        assertEquals(ResponseStatus.SUCCESS, universListResource.getStatus());
        assertEquals("200", universListResource.getMessageCode());
        assertEquals("The call was executed correctly.", universListResource.getMessage());
    }

    private void assertEqualWithAnEmptyListOfUniversResp(UniversListResource universListResource) {
        assertTrue(universListResource.getUnivers().isEmpty());
        assertEquals(ResponseStatus.SUCCESS, universListResource.getStatus());
        assertEquals("200", universListResource.getMessageCode());
        assertEquals("The call was executed correctly.", universListResource.getMessage());
    }

    private List<Univers> buildAListOfUnivers(double... peso){
        DoubleStream doubleStream = DoubleStream.of(peso);
        List<Univers> result = doubleStream.mapToObj(d -> buildAnOptionalUnivers(d).get())
                .collect(Collectors.toList());
        return result;
    }

    private List<Univers> buildAnEmptyListOfUnivers() {
        Stream<Univers> stream = Stream.empty();
        return stream.collect(Collectors.toList());
    }

    private void assertEqualsUniversCommonField(Univers univers, UniversResp universResp) {
        assertEquals(univers.getId(), universResp.getId());
        assertEquals(univers.getVersion(), universResp.getVersion());
        assertEquals(univers.getCreateBy(), universResp.getCreateBy());
        assertEquals(univers.getCreatedDate(), universResp.getCreatedDate());
        assertEquals(univers.getPeso(), universResp.getPeso());
        assertEquals(univers.getRoutePhase(), universResp.getRoutePhase());
        assertEquals(univers.getStatus(), universResp.getStatus());
    }

    private void assertEqualsEmptyOptionalUnivers(UniversResource actual) {
        assertEquals(ResponseStatus.WARN, actual.getStatus());
        assertEquals("500", actual.getMessageCode());
        assertEquals("Non esiste un universo con tali dati.", actual.getMessage());
        assertNull(actual.getUniversResp());
    }

    private void assertEqualsOptionalUnivers(UniversResource actual) {
        assertEquals(ResponseStatus.SUCCESS, actual.getStatus());
        assertEquals("200", actual.getMessageCode());
        assertEquals(OK_CALL, actual.getMessage());
        assertNotNull(actual.getUniversResp());
    }

    private List<Person> buildAListOfPersons(String... names){
        List<Person> result = Stream.of(names)
                                    .map(name -> buildAPerson(name))
                                    .collect(Collectors.toList());
        return result;
    }

    private Person buildAPerson(String name){
        Person result = Person.of(Instant.now(), name, null, null, PERSON_NAME,
                PERSON_SURNAME, EMAIL, PHONE, null);
        return result;
    }

    private Optional<Univers> buildAnOptionalUnivers(double peso){
        List<Person> result = buildAnEmptyListOfPerson();
        return buildAnOptionalUnivers(result, peso);
    }

    private List<Person> buildAnEmptyListOfPerson() {
        Stream<Person> stream = Stream.empty();
        return stream.collect(Collectors.toList());
    }

    private Optional<Univers> buildAnOptionalUnivers(List<Person> persons, double peso){
        Univers result = Univers.builder()
                                .createBy(ME)
                                .createdDate(LocalDate.now())
                                .peso(BigDecimal.valueOf(peso))
                                .persons(persons)
                                .routePhase(ROUTE_PHASE_A)
                                .status(UniversStatus.COMPLETED)
                                .build();
        return Optional.of(result);
    }

    private void assertEqualOfUpdatedFields(UniversResource actual) {
        assertEquals(ROUTE_PHASE_B, actual.getUniversResp().getRoutePhase());
        assertEquals(UniversStatus.SCHEDULING, actual.getUniversResp().getStatus());
        assertEquals(new BigDecimal(12345.01), actual.getUniversResp().getPeso());
    }

    private void mockSaveUnivers(UniversReq universReq) {
        Univers universUpdated = buildAnOptionalUnivers(11111.01).get();
        universUpdated.updateUnivers(universReq);
        Mockito.doReturn(universUpdated).when(universRepositoryJpa).save(universUpdated);
    }

    private void mockGetOneUnivers() {
        Univers univers = buildAnOptionalUnivers(11111.01).get();
        Mockito.doReturn(univers).when(universRepositoryJpa).getOne(1L);
    }
}