package prv.inv.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import prv.inv.api.E2ETests;
import prv.inv.api.enums.UniversStatus;
import prv.inv.api.model.*;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static prv.inv.api.util.PrvInvConstants.OK_CALL;
import static prv.inv.api.util.PrvInvConstants.OK_SAVE_CALL;

@Slf4j
@Category(E2ETests.class)
@SpringBootTest
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class UniversPrvInvMsTest {


    @Autowired
    private ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    @Autowired
    protected UniversPrvInvMs universPrvInvMs;
    @Autowired
    protected WebApplicationContext wac;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void getAllUnivers() throws Exception{
        MvcResult mvcResult = mockMvc.perform(get("http://localhost:8080/univers")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        UniversListResource universListResource = objectMapper.readValue(
                mvcResult.getResponse().getContentAsString(), UniversListResource.class);
        universListResource.getUnivers().forEach(universResp -> log.info("Univers id: {}",universResp.getId()));
    }


    @Test
    public void createAUnivers() throws Exception{
        UniversReq universReq = UniversReq.of(UniversStatus.COMPLETED,"A", BigDecimal.ONE);
        String json = objectMapper.writeValueAsString(universReq);
        MvcResult result = mockMvc.perform(post("http://localhost:8080/univers")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(json))
                            .andExpect(status().isOk())
                            .andReturn();

        UniversResource response = objectMapper.readValue(result.getResponse().getContentAsString(), UniversResource.class);

        assertEquals(ResponseStatus.SUCCESS, response.getStatus());
        assertEquals("200", response.getMessageCode());
        assertEquals(OK_SAVE_CALL, response.getMessage());
        assertEquals(UniversStatus.COMPLETED, response.getUniversResp().getStatus());
        assertEquals(BigDecimal.ONE, response.getUniversResp().getPeso());
        assertEquals("A", response.getUniversResp().getRoutePhase());
    }
}