<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="template/includes.jsp" %>
<!doctype html>
<head>
    <title>prv-app-login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" id="color-scheme" href="css/colors/salmon.css">

    <script src="js/modernizr.js"></script>
    <script src="js/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6" style="padding-top: 20%;">
            <h2 class="title-underblock custom mb40">Prv Invoice Api</h2>
            <ul>
                <li>
                    <a href="https://gitlab.com/elion.haxhi/prv-inv-api/">Source Code</a>
                </li>
                <li>
                    <a href="/prv-inv-api/swagger-ui.html">Swagger</a>
                </li>
            </ul>
        </div>

        <div class="mb40 visible-xs"></div>

        <div class="col-sm-6" style="padding-top: 20%">
            <h2 class="title-underblock custom mb40">Information</h2>
            <p>This webapp is created to make e preview of some rest web service.
                The app is build with java 8 ,lombok, and the spring-boot framework.
                The app also make use of spring-data-jpa to integrate with the database.
                The script to build the database schema is located under the resources folder
                and the db.migration. You can build the schema in mysql or in postgres if you want.
                If you want to take a look al the rest endpoint you should click on the swagger link
                after that you should be redirected to another system which is responsible for the
                authentication.
                Once you put the username and password correctly then you can navigate throw swagger.
                During the creation of this app i made use of the TDD methodology, which is interesting and i
                advice
                that a lot. Shifting gears you can take a look at the source code if you want. To do that just
                click on the link
                Source Code on the right side of the home page. </p>

            <p>Thank you for being here!</p>
        </div>
    </div><!-- End of div Row -->
    <div class="mb35 mb20-xs"></div><!-- space -->

    <div class="bg-image overlay-container" data-bgattach="images/backgrounds/index7/bg1.jpg">
        <div class="overlay dark"></div><!-- End .overlay -->
        <div class="container">
            <div class="iframe-btn-container">
                <header class="title-block text-center mb50">
                    <h2 class="text-white text-center mb20">Watch the Video..</h2>
                    <p class="text-white">Please check out the video below to find what you are looking for. <br> Then feel free to get in touch with me!</p>
                </header>
                <a href="https://www.youtube.com/watch?v=kBWoq7zWCSU" class="popup-iframe iframe-btn" title="Our Video"><i class="icon-control-play"></i></a>
            </div><!-- End .iframe-btn-container -->
        </div><!-- End .container -->
    </div><!-- End .bg-image -->

    <div class="mb65 mb50-xs"></div><!--space -->
</div><!-- End of div Container -->
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/main.js"></script>
</body>
</html>
