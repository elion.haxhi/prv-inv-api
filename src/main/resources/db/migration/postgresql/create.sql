CREATE TABLE UNIVERS (
       ID BIGSERIAL NOT NULL,
       VERSION integer,
       CREATEBY character varying(255) NOT NULL,
       CREATEDATE date NOT NULL,
       UPDATEBY character varying(255) NOT NULL,
       UPDATEDATE timestamp without time zone NOT NULL,
       STATUS varchar(20),
       ROUTEPHASE varchar(255),
       PESO numeric(19,2) NOT NULL,
       PRIMARY KEY (ID)
);

CREATE TABLE PERSON (
     ID BIGSERIAL NOT NULL,
     VERSION integer,
     CREATEBY character varying(255) NOT NULL,
     CREATEDATE timestamp without time zone NOT NULL,
     UPDATEBY character varying(255) NOT NULL,
     UPDATEDATE timestamp without time zone NOT NULL,
     NAME varchar(255) not null,
     SURNAME varchar(255) not null,
     EMAILADDRESS varchar(255),
     PHONENUMBER varchar(20),
     SEC_USER_ID bigint not null,
     UNIVERS_ID bigint not null,
     PRIMARY KEY (ID)
);

ALTER TABLE PERSON ADD CONSTRAINT FK_P_U FOREIGN KEY (UNIVERS_ID) REFERENCES UNIVERS(ID);