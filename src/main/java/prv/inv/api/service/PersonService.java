package prv.inv.api.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import prv.inv.api.entity.Person;
import prv.inv.api.exception.UniversException;
import prv.inv.api.model.PersonListResource;
import prv.inv.api.model.PersonResource;
import prv.inv.api.model.PersonResp;
import prv.inv.api.model.ResponseStatus;
import prv.inv.api.repository.PersonRepository;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class PersonService {

    private PersonRepository personRepository;

    private AuthorizationService authorizationService;

    public PersonService(@Qualifier(PersonRepository.PERSON_REPOSITORY) PersonRepository personRepository,AuthorizationService authorizationService){
        this.personRepository = personRepository;
        this.authorizationService = authorizationService;
    }


    public PersonResource retrieveAPersonResourceById(String token, long id) throws NoSuchAlgorithmException{

        if(authorizationService.isAuthenticated(token)){
            PersonResp personResp = convertToPersonResp(findPerson(id));
            PersonResource resourceResult = PersonResource.of(personResp);

            updatePersonResource(personResp, resourceResult);

            return resourceResult;
        }
        else {
            throw new UniversException("Token credential are not present on db");
        }
    }

    private void updatePersonResource(PersonResp personResp, PersonResource resourceResult) {
        resourceResult.setStatus(ResponseStatus.SUCCESS);
        resourceResult.setMessage("La chiamata è andata a buon fine");
        resourceResult.setMessageCode("200");
        resourceResult.setPersonResp(personResp);
    }

    public PersonResp findByCreateDateAndId(LocalDate localDate, long id){
        Person person = personRepository.findByCreateDateAndId(localDate, id).get(0);
        return convertToPersonResp(person);
    }

    public PersonListResource retrievePersonListResourceByUniversId(String token, long idUniverso)throws NoSuchAlgorithmException {

        if(authorizationService.isAuthenticated(token)){
            PersonListResource personListResource = new PersonListResource();
            List<Person> personEntities = personRepository.trovaTutteLePersonPerUnivers(idUniverso);
            List<PersonResp> personRespList = personEntities.stream()
                    .map(this::convertToPersonResp).collect(Collectors.toList());

            personListResource.setStatus(ResponseStatus.SUCCESS);
            personListResource.setPerson(personRespList);
            personListResource.setMessage("La chiamata e andata a buon fine");
            personListResource.setMessageCode("200");

            return personListResource;
        } else {
            throw new UniversException("Token credential are not present on db");
        }

    }

    private Person findPerson(long id){
        Optional<Person> person = personRepository.findById(id);
        return person.orElseThrow(RuntimeException::new);
    }

    private PersonResp convertToPersonResp(Person person){
        return PersonResp.builder()
                .toPersonResp(person)
                .build();
    }

}
