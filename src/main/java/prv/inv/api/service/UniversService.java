package prv.inv.api.service;

import com.querydsl.core.types.Predicate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import prv.inv.api.entity.Person;
import prv.inv.api.entity.Univers;
import prv.inv.api.exception.UniversException;
import prv.inv.api.model.*;
import prv.inv.api.repository.UniversRepository;
import prv.inv.api.repository.UniverseEntityQueryBuilder;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static prv.inv.api.util.PrvInvConstants.*;


@Service
public class UniversService {

    private final UniversRepository universRepository;
    private final AuthorizationService authorizationService;
    private final PersonService personService;


    public UniversService(UniversRepository universRepository,
                          AuthorizationService authorizationService,
                          PersonService personService) {
        this.universRepository = universRepository;
        this.authorizationService = authorizationService;
        this.personService = personService;
    }

    public UniversListResource queryUniversesByParameters(String token,
                                                          Predicate predicate,
                                                          LocalDate createdDate,
                                                          BigDecimal peso, String... params)
            throws NoSuchAlgorithmException {
        return queryUniverses(token, predicate, null, createdDate, peso, params);
    }

    public UniversListResource queryUniverses(String query, String token)
            throws NoSuchAlgorithmException {

        return queryUniverses(token, null, query, null, null, null);
    }

    public UniversListResource queryUniverses(String token, Predicate predicate, String query,
                                              LocalDate createdDate,
                                              BigDecimal peso, String[] params) throws NoSuchAlgorithmException {
        if (authorizationService.isAuthenticated(token)) {
            return createUniverseList(
                    wrapToUniversRespList(
                            fetchUniverses(predicate, query, createdDate, peso, params)));
        } else {
            throw new UniversException(TOKEN_VALID_BUT_USER_NOT_EXIST);
        }
    }

    public List<PersonResp> wrapToPersonResp(List<Person> personList) {
        Stream<PersonResp> stream = Stream.empty();
        if (personList != null)
            return personList.stream()
                    .map(person -> PersonResp.builder().toPersonResp(person).build())
                    .collect(Collectors.toList());

        return stream.collect(Collectors.toList());
    }

    public List<UniversResp> wrapToUniversRespList(List<Univers> universes) {
        return universes.stream()
                .map(u -> UniversResp.builder()
                        .univers(u)
                        .persons(wrapToPersonResp(u.getPersons()))
                        .build())
                .collect(Collectors.toList());
    }

    public List<Univers> fetchAllUniverses() {
        return universRepository.findAll();
    }

    @Transactional
    public UniversResource deleteUniverse(String token, long id) throws NoSuchAlgorithmException {
        if (authorizationService.isAuthenticated(token)) {
            UniversResource response = new UniversResource();
            universRepository.deleteById(id);
            response.setStatus(ResponseStatus.SUCCESS);
            response.setMessageCode("200");
            response.setMessage(OK_CALL);
            return response;
        } else {
            throw new UniversException("Token credential are not present on db");
        }
    }

    public UniversResource createUnivers(String token, UniversReq ur) throws NoSuchAlgorithmException {
        if (authorizationService.isAuthenticated(token)) {
            String user = authorizationService.decodeToken(token).get("user");
            Univers univers = Univers.builder().build().wrapToUnivers(ur, user);
            return generateUniversResourceFromUniversResp(wrapToUniversResp(saveAndReturn(univers)), OK_SAVE_CALL);
        } else {
            throw new UniversException(TOKEN_VALID_BUT_USER_NOT_EXIST);
        }
    }

    @Transactional
    public UniversResource updateUnivers(String token, UniversReq r, long id) throws NoSuchAlgorithmException {
        if (authorizationService.isAuthenticated(token)) {
            Univers univers = universRepository.getOne(id);
            univers.setStatus(r.getStatus());
            univers.setPeso(r.getPeso());
            univers.setRoutePhase(r.getRoutePhase());

            universRepository.save(univers);

            return generateUniversResourceFromUniversResp(wrapToUniversResp(univers), OK_UPDATE_CALL);
        } else {
            throw new UniversException("Token credential are not present on db");
        }
    }

    public UniversResource generateUniversResourceFromUniversResp(UniversResp universResp, String message) {
        UniversResource resource = new UniversResource();
        resource.setUniversResp(universResp);
        resource.setStatus(ResponseStatus.SUCCESS);
        resource.setMessageCode("200");
        resource.setMessage(message);
        return resource;
    }

    public UniversResource generateUniversResourceWithError(String message) {
        UniversResource resource = new UniversResource();
        resource.setUniversResp(null);
        resource.setStatus(ResponseStatus.ERROR);
        resource.setMessageCode("500");
        resource.setMessage(message);
        return resource;
    }

    @Transactional(readOnly = true)
    public UniversResource getUniverse(long id, String token) throws NoSuchAlgorithmException {
        if (authorizationService.isAuthenticated(token))
            return generateResponse(universRepository.findById(id));
        else
            throw new UniversException(TOKEN_VALID_BUT_USER_NOT_EXIST);
    }

    public UniversResource generateResponse(Optional<Univers> univers) {
        UniversResource response = new UniversResource();
        if (univers.isPresent())
            createSuccessUniversResource(response, wrapToUniversResp(univers.get()));
        else
            createWarnUniversResource(response);
        return response;
    }

    public UniversResp wrapToUniversResp(Univers univers) {
        return UniversResp.builder()
                .univers(univers)
                .persons(wrapToPersonResp(univers.getPersons()))
                .build();
    }

    public UniversListResource getUniverses(String token) throws NoSuchAlgorithmException {
        if (authorizationService.isAuthenticated(token)) {
            return createUniverseList(wrapToUniversRespList(fetchAllUniverses()));
        } else {
            throw new UniversException(TOKEN_VALID_BUT_USER_NOT_EXIST);
        }
    }

    public UniversListResource createUniverseList(List<UniversResp> universResps) {
        UniversListResource result = UniversListResource.builder()
                .univers(universResps)
                .build();
        result.setStatus(ResponseStatus.SUCCESS);
        result.setMessage(OK_CALL);
        result.setMessageCode("200");
        return result;
    }

    public PersonListResource getPersonListResourceByUniversId(String authorization, long id) throws NoSuchAlgorithmException {
        return personService.retrievePersonListResourceByUniversId(authorization, id);
    }

    private UniverseEntityQueryBuilder setUpQueryBuilder(String query, LocalDate createdDate, BigDecimal peso, String[] params) {
        UniverseEntityQueryBuilder builder = new UniverseEntityQueryBuilder();
        if (query != null)
            builder.encodeString(query);
        else
            builder.encodeParameters(createdDate, peso, params[0], params[1]);
        return builder;
    }

    private List<Univers> fetchUniverses(Predicate predicate, String query, LocalDate createdDate, BigDecimal peso, String[] params) {
        UniverseEntityQueryBuilder builder = setUpQueryBuilder(query, createdDate, peso, params);
        return (List<Univers>) universRepository.findAll(builder.build(predicate));
    }

    private void createWarnUniversResource(UniversResource response) {
        response.setStatus(ResponseStatus.WARN);
        response.setMessage(NO_UNIVERS);
        response.setMessageCode("500");
    }

    private void createSuccessUniversResource(UniversResource response, UniversResp universResp) {
        response.setUniversResp(universResp);
        response.setStatus(ResponseStatus.SUCCESS);
        response.setMessageCode("200");
        response.setMessage(OK_CALL);
    }

    private Univers saveAndReturn(Univers univers) {
        return universRepository.save(univers);
    }

}
