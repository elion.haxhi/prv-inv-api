package prv.inv.api.service;

import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.util.JSONPObject;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import prv.inv.api.exception.PayloadException;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class AuthorizationService{

    public static final Logger log = Logger.getLogger(AuthorizationService.class.getName());

    public static final String USER = "elion";
    public static final String PASSWORD = "admin";
    public static final String ISSUER = "www.elionhaxhi.com";
    private static final String SECRET_KEY = "MY_SIGNATURE";


    private Map<String,String> payload = new HashMap<>();
    private Map<String,String> header = new HashMap<>();
    private String signature;




    public AuthorizationService(){
        createHeader();
        createPayload();
    }


    public Map<String, String> createPayload() {
        payload.put("user", USER);
        payload.put("password", PASSWORD);
        payload.put("iss", ISSUER);

        return payload;
    }

    public String generateToken() {
        String encodedHeader = encode(header);
        String encodedPayload = encode(payload);
        String signature = hmacSha256(encodedHeader,encodedPayload);
        return encodedHeader + "." + encodedPayload + "." + signature;

    }

    public static String encode(Map<String, String> map) {

        return encode(map.toString().getBytes());
    }

    private static String encode(byte[] bytes) {

        return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
    }

    public Map<String, String> createHeader() {
        header.put("alg", "HS256");
        header.put("typ", "JWT");

        return header;
    }

    public String hmacSha256(String header, String payload) {

        try {
            String data = header.concat(".").concat(payload);

            byte[] hash = SECRET_KEY.getBytes(StandardCharsets.UTF_8);

            Mac sha256Hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(hash, "HmacSHA256");
            sha256Hmac.init(secretKey);

            byte[] signedBytes = sha256Hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));
            return encode(signedBytes);
        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            log.info(ex.getMessage());
            return null;
        }
    }

    public Map<String, String> decodeToken(String token) throws NoSuchAlgorithmException{

        String[] parts = token.split("\\.");
        String encodedHeader = encode(header);

        if (parts.length != 3)
            throw new IllegalArgumentException("JWT Invalid Token format");
        if (!encodedHeader.equals(parts[0]))
            throw new NoSuchAlgorithmException("JWT Header is Incorrect: " + parts[0]);

        payload = decode(parts[1]);

        if(!encode(payload).equals(parts[1]))
            throw new PayloadException("JWT Payload is Incorrect:" + parts[1]);

        return payload;
    }

    public Map<String, String> decode(String encodedPayLoad) {

        String plain = new String(Base64.getUrlDecoder().decode(encodedPayLoad));
        String plainResult = plain.substring(1,plain.length()-1);
        return  Arrays.stream(plainResult.split(", "))
                      .map(entry -> entry.split("="))
                      .collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));

    }

    public boolean isAuthenticated(String token) throws NoSuchAlgorithmException{
        Map<String, String> payload = decodeToken(token);
            return payload.get("user").equals(USER) && payload.get("password").equals(PASSWORD);
    }
}
