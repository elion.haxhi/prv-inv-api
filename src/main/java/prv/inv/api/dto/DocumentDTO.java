package prv.inv.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class DocumentDTO {

    @JsonProperty("id_document")
    @ApiModelProperty(required = true, value = "L'id del document")
    private long idDocument;

    public long getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(long idDocument) {
        this.idDocument = idDocument;
    }
}
