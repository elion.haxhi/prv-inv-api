package prv.inv.api.repository;

import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import prv.inv.api.entity.QUnivers;

public class UniverseEntityQuerydlsBinding implements QuerydslBinderCustomizer<QUnivers> {

    @Override
    public void customize(QuerydslBindings bindings, QUnivers root) {

        bindings.bind(String.class)
                .first((StringPath path,String value) -> path.containsIgnoreCase(value));
    }
}
