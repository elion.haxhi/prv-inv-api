package prv.inv.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import prv.inv.api.entity.Univers;
import prv.inv.api.enums.UniversStatus;

import java.math.BigDecimal;
import java.time.LocalDate;

@Repository
public interface UniversRepository extends AbstractRepository<Univers, Long>, QuerydslPredicateExecutor<Univers> {


    @Query("select u from Univers u where u.status = :status " +
            "and u.routePhase = :routePhase " +
            "and u.createdDate = :createdDate " +
            "and u.peso = :peso")
    Univers readData(@Param("status") UniversStatus status, @Param("routePhase") String routePhase, @Param("createdDate") LocalDate createdDate, @Param("peso") BigDecimal peso);



}
