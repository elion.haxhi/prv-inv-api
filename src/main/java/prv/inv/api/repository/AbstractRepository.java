package prv.inv.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@NoRepositoryBean
@Transactional(readOnly = true)
public interface AbstractRepository<T, A extends Serializable> extends JpaRepository<T, A> {
}
