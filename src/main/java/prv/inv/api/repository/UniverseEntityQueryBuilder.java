package prv.inv.api.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import prv.inv.api.entity.QUnivers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class UniverseEntityQueryBuilder {

    private String query;
    private String status;
    private String routePhase;
    private LocalDate createdDate;
    private BigDecimal peso;
    DateTimeFormatter datefomater = DateTimeFormatter.ISO_DATE;

    public void encodeParameters(LocalDate createdDate, BigDecimal peso, String status, String routePhase) {

        if (createdDate != null) {
            this.createdDate = createdDate;
        }
        if (status != null && !status.isEmpty()) {
            this.status = status;
        }
        if (routePhase != null && !routePhase.isEmpty()) {
            this.routePhase = routePhase;
        }
        if (peso != null) {
            this.peso = peso;
        }

    }

    public void encodeString(String query) {
        this.query = query;
    }

    public void encodeParameters(MultiValueMap<String, String> parameters) {


        if (parameters.containsKey("created_date")) {
            List<String> value = parameters.get("created_date");
            if (value.size() == 1) {
                createdDate = LocalDate.parse(value.get(0), datefomater);
            }
        }

        if (parameters.containsKey("query")) {
            List<String> value = parameters.get("query");
            if (value.size() == 1) {
                query = value.get(0);
            }
        }

        if (parameters.containsKey("status")) {
            List<String> value = parameters.get("status");
            if (value.size() == 1) {
                status = value.get(0);
            }
        }

        if (parameters.containsKey("route_phase")) {
            List<String> value = parameters.get("route_phase");
            if (value.size() == 1) {
                routePhase = value.get(0);
            }
        }

        if (parameters.containsKey("peso")) {
            List<String> value = parameters.get("peso");
            if (value.size() == 1) {
                peso = new BigDecimal(value.get(0));
            }
        }

    }

    public Predicate build(Predicate predicate) {
        BooleanBuilder builder = new BooleanBuilder(predicate);
        QUnivers universes = QUnivers.univers;
        fillTheBuilder(builder, universes);
        if (status != null)
            builder.and(universes.status.stringValue().containsIgnoreCase(status));
        if (routePhase != null)
            builder.and(universes.routePhase.containsIgnoreCase(routePhase));
        if (createdDate != null)
            builder.and(universes.createdDate.goe(createdDate));
        if (peso != null)
            builder.and(universes.peso.eq(peso));

        return builder;
    }

    private void fillTheBuilder(BooleanBuilder builder, QUnivers universes) {
        if (!StringUtils.isEmpty(query)) {

            BooleanBuilder nameBuilder = new BooleanBuilder();
            String[] arr = query.split(" ");

            if (arr == null) {
                arr = new String[]{query};
            }
            for (String part : arr) {
                nameBuilder.and(
                        new BooleanBuilder()
                                .or(universes.routePhase.containsIgnoreCase(part))
                                .or(universes.status.stringValue().containsIgnoreCase(part))
                                .or(universes.peso.stringValue().containsIgnoreCase(part))
                );
            }

            builder.and(nameBuilder);
        }
    }
}


























