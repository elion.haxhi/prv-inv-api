package prv.inv.api.repository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import prv.inv.api.entity.Person;

import java.time.LocalDate;
import java.util.List;

@Repository
@Qualifier(value = PersonRepository.PERSON_REPOSITORY)
public interface PersonRepository extends AbstractRepository<Person, Long> {

    public static final String PERSON_REPOSITORY = "PersonRepository";

    List<Person> findByCreateDateAndId(LocalDate localDate, long id);

    @Query("select ep from Person ep where ep.univers.id = :idUniverso")
    List<Person> trovaTutteLePersonPerUnivers(@Param("idUniverso") long idUniverso);

}
