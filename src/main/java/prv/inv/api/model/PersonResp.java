package prv.inv.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import prv.inv.api.entity.Person;

import java.time.Instant;
@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "PersonResp", description = "Visualiza tutti i dati personali di una person")
public class PersonResp {

    @JsonProperty("id_person")
    @ApiModelProperty(required = false, value = "L'id del person richiesto")
    private Long idPerson;

    @JsonProperty("create_date")
    @ApiModelProperty(required = true, value = "La data di registrazione della person")
    private Instant createDate;

    @JsonProperty("create_by")
    @ApiModelProperty(required = true, value = "L'utente che ha registrato la person")
    private String createBy;

    @JsonProperty("last_modified_date")
    @ApiModelProperty(required = true, value = "La data di modifica registrazione")
    private Instant lastModifiedDate;

    @JsonProperty("last_modified_by")
    @ApiModelProperty(required = true, value = "L'utente che ha modificato la registrazione della person")
    private String lastModifiedBy;

    @JsonProperty("name")
    @ApiModelProperty(required = true, value = "Il nome della person")
    private String name;

    @JsonProperty("surname")
    @ApiModelProperty(required = true, value = "Il cognome della person")
    private String surname;

    @JsonProperty("email")
    @ApiModelProperty(required = true, value = "L'indirizzo email della person")
    private String email;

    @JsonProperty("phone")
    @ApiModelProperty(required = true, value = "Il numero del telefono della person")
    private String phone;

    @Builder
    private PersonResp(Person toPersonResp){
        this.idPerson = toPersonResp.getId() != null ? toPersonResp.getId() : null;
        this.createBy = toPersonResp.getCreateBy() != null ? toPersonResp.getCreateBy() : null;
        this.createDate = toPersonResp.getCreateDate() != null ? toPersonResp.getCreateDate() : null;
        this.lastModifiedDate = toPersonResp.getLastModifiedDate() != null ? toPersonResp.getLastModifiedDate() : null;
        this.lastModifiedBy = toPersonResp.getLastModifiedBy() != null ? toPersonResp.getLastModifiedBy() : null;
        this.name = toPersonResp.getName() != null ? toPersonResp.getName() : null;
        this.surname = toPersonResp.getSurname() !=null ? toPersonResp.getSurname() : null;
        this.phone = toPersonResp.getPhone() !=null ? toPersonResp.getPhone() : null;
        this.email = toPersonResp.getEmail() !=null ? toPersonResp.getEmail() : null;
    }
}
