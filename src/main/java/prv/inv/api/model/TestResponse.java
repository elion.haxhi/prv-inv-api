package prv.inv.api.model;
import org.joda.time.DateTimeUtils;
import java.sql.Timestamp;

public class TestResponse {

	String message;
	Object value;
	Timestamp date = new Timestamp(DateTimeUtils.currentTimeMillis());

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Timestamp getDate() {
		return date;
	}

}
