package prv.inv.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import prv.inv.api.enums.UniversStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@ApiModel(description = "Questa classe rapresenta i campi neccessari per salvare in universo")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class UniversReq implements Serializable {

    @JsonProperty(value = "status" ,required = true, defaultValue = "STARTED")
    @ApiModelProperty(required = true, value = "Lo stato del universo")
    private UniversStatus status;

    @JsonProperty(value = "route_phase", required = true, defaultValue = "A")
    @ApiModelProperty(required = true, value = "La fase di route")
    private String routePhase;

    @JsonProperty(value = "peso", required = true ,defaultValue = "11111.01")
    @ApiModelProperty(required = true, value = "Il peso del universo")
    private BigDecimal peso;

}
