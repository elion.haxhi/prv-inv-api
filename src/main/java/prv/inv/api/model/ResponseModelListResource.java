package prv.inv.api.model;

import java.util.Collection;

public class ResponseModelListResource<T> extends ResponseStatusResource {

	private Collection<T> entities;

	public Collection<T> getEntities() {
		return entities;
	}

	public void setEntities(Collection<T> entities) {
		this.entities = entities;
	}
}
