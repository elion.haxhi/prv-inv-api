package prv.inv.api.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PersonResource extends StatusResource{

    private PersonResp personResp;

}
