package prv.inv.api.model;

public enum ResponseStatus {
	UNDEF,
	SUCCESS,
	ERROR,
	WARN
}
