package prv.inv.api.model;

import java.util.List;

public class PersonListResource extends StatusResource{

    private List<PersonResp> personRespons;

    public List<PersonResp> getPerson() {
        return personRespons;
    }

    public void setPerson(List<PersonResp> personRespons) {
        this.personRespons = personRespons;
    }
}
