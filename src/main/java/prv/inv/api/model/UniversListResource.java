package prv.inv.api.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UniversListResource extends StatusResource{

    private List<UniversResp> univers;

}
