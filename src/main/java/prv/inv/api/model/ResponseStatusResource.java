package prv.inv.api.model;

import org.springframework.validation.ObjectError;
import java.util.HashMap;
import java.util.List;

public class ResponseStatusResource {

	ResponseStatus status = ResponseStatus.UNDEF;
	
	String messageCode;
	String message;
	
	List<ObjectError> errors;
	
	HashMap<String, Object> responseData = new HashMap<String, Object>();

	public ResponseStatusResource(){

	}

	public ResponseStatusResource(ResponseStatus responseStatus){
		status = responseStatus;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}
	
	public void setSuccess(){
		status = ResponseStatus.SUCCESS;
	}
	
	public void setError(){
		status = ResponseStatus.ERROR;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public HashMap<String, Object> getResponseData() {
		return responseData;
	}

	public void setResponseData(HashMap<String, Object> responseData) {
		this.responseData = responseData;
	} 
	
	public void addResponseData(String key, Object value){
		this.responseData.put(key, value);
	}
	
	public List<ObjectError> getErrors() {
		return errors;
	}

	public void setErrors(List<ObjectError> errors) {
		this.errors = errors;
		setError();
	}

}
