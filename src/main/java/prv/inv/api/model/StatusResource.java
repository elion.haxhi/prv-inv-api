package prv.inv.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.ObjectError;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class StatusResource {

	ResponseStatus status = ResponseStatus.UNDEF;
	String messageCode;
	String message;
	List<ObjectError> errors;


}
