package prv.inv.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import prv.inv.api.entity.Univers;
import prv.inv.api.enums.UniversStatus;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@ApiModel(description = "Questa classe rapresenta l'oggetto Universo")
@Getter
@Setter
@NoArgsConstructor
public class UniversResp {

    @JsonProperty("id")
    @ApiModelProperty(value = "Il valore univoco del id")
    private Long id;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonProperty("created_date")
    @ApiModelProperty(required = true, value = "La data di creazione")
    private LocalDate createdDate;

    @JsonProperty("version")
    @ApiModelProperty(value = "Il valore della versione")
    private Integer version;

    @JsonProperty("create_by")
    @ApiModelProperty(required = true, value = "Lo username del utente che lo ha creato")
    private String createBy;

    @JsonProperty("last_modified_date")
    @ApiModelProperty(value = "La data di modifica")
    private Instant lastModifiedDate;

    @JsonProperty("last_modified_by")
    @ApiModelProperty(required = true, value = "Lo username del utente che lo ha aggiornato")
    private String lastModifiedBy;

    @JsonProperty("status")
    @ApiModelProperty(required = false, value = "Lo stato del universo")
    private UniversStatus status;

    @JsonProperty("route_phase")
    @ApiModelProperty(required = false, value = "La fase di route")
    private String routePhase;

    @JsonProperty("peso")
    @ApiModelProperty(required = false, value = "Il peso del universo")
    private BigDecimal peso;

    @JsonProperty("persons")
    @ApiModelProperty(required = false, value = "La lista delle persone che fanno parte di questo universo")
    private List<PersonResp> persons;
    
    @Builder
    private UniversResp (Univers univers, List<PersonResp> persons){
        this.id = univers.getId() != null ? univers.getId() : null;
        this.createdDate = univers.getCreatedDate() != null ? univers.getCreatedDate() : null;
        this.version = univers.getVersion() != null ? univers.getVersion() :null;
        this.createBy = univers.getCreateBy() != null ? univers.getCreateBy() : null;
        this.lastModifiedDate = univers.getLastModifiedDate() != null ? univers.getLastModifiedDate() : null;
        this.lastModifiedBy = univers.getLastModifiedBy() != null ? univers.getLastModifiedBy() :null;
        this.status = univers.getStatus() != null ? univers.getStatus() : null;
        this.routePhase = univers.getRoutePhase() != null ? univers.getRoutePhase() : null;
        this.peso = univers.getPeso() != null ? univers.getPeso() : null;
        this.persons = persons;
    }

}
