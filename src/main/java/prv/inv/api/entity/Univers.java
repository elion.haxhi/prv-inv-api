package prv.inv.api.entity;

import lombok.*;
import prv.inv.api.entity.core.IdAndVersionEntity;
import prv.inv.api.enums.UniversStatus;
import prv.inv.api.model.UniversReq;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = Univers.UNIVERS_ENTITY)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
@ToString(callSuper = true)
@Builder
public class Univers extends IdAndVersionEntity {

    public static final String UNIVERS_ENTITY = "univers";
    private static final long serialVersionUID = 4846595815091238816L;

    @Column(nullable = false, name="create_date", insertable=true)
    private LocalDate createdDate;

    @Column(nullable = false, name="create_by", insertable=true, updatable=false)
    private String createBy;

    @Column(name="update_date" )
    private Instant lastModifiedDate;

    @Column(name="update_by")
    private String lastModifiedBy;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "status")
    private UniversStatus status;

    @Column(nullable = false, name = "route_phase")
    private String routePhase;

    @Column(nullable = false, name = "peso")
    private BigDecimal peso;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "univers")
    private List<Person> persons;


    public Univers wrapToUnivers(UniversReq universReq, String user){

        return Univers.builder()
                .createBy(user)
                .createdDate(LocalDate.now())
                .status(universReq.getStatus())
                .peso(universReq.getPeso())
                .routePhase(universReq.getRoutePhase())
                .build();
    }

    public void updateUnivers(UniversReq universReq){
        this.status = universReq.getStatus();
        this.peso = universReq.getPeso();
        this.routePhase = universReq.getRoutePhase();
    }


}
