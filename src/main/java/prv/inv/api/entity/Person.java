package prv.inv.api.entity;

import lombok.*;
import prv.inv.api.entity.core.IdAndVersionEntity;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = Person.PERSON_ENTITY)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
@ToString
public class Person extends IdAndVersionEntity {

    private static final long serialVersionUID = 4846595815091238816L;
    public static final String PERSON_ENTITY = "person";

    @Column(nullable=false, name="create_date", insertable=true, updatable=false)
    private Instant createDate;

    @Column(nullable=false, name="create_by", insertable=true, updatable=false)
    private String createBy;

    @Column(name="update_date" )
    private Instant lastModifiedDate;

    @Column(name="update_by")
    private String lastModifiedBy;

    @Column(nullable=false, name="name")
    private String name;

    @Column(nullable=false, name="surname")
    private String surname;

    @Column(nullable=false, name="email_address")
    private String email;

    @Column(nullable=false, name="phone_number")
    private String phone;

    @ManyToOne
    @JoinColumn(nullable = false, name = "univers_id")
    private Univers univers;

}
