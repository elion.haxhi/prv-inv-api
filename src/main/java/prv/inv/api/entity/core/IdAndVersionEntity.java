package prv.inv.api.entity.core;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;


@MappedSuperclass
@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public abstract class IdAndVersionEntity implements Serializable {

	private static final long serialVersionUID = -2101490886258519798L;

	@JsonView(IdAndVersionView.IdAndVersion.class)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonView(IdAndVersionView.IdAndVersion.class)
	@Column
	@Version
	private Integer version;

}