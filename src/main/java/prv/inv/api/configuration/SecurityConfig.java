package prv.inv.api.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${prv.app.login.host}")
	private String hostAddress;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder();
	}

	@Bean
	public FilterRegistrationBean<FrontControllerFilter> frontFilter() throws Exception {
		FilterRegistrationBean<FrontControllerFilter> registrationBean
				= new FilterRegistrationBean<>();
		registrationBean.setFilter(new FrontControllerFilter(authenticationManagerBean(),hostAddress));
		registrationBean.addUrlPatterns("/swagger-ui.html");

		return registrationBean;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/static/**").permitAll()
				.antMatchers("/**").permitAll()
				.antMatchers("/logout2").permitAll()
				.antMatchers("/swagger-ui.html").permitAll()
				.antMatchers("/api/**").permitAll();
		http.csrf().disable();

	}

	@Bean(name = BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}


}
