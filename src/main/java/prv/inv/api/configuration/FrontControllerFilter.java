package prv.inv.api.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.BeanIds;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@Slf4j
public class FrontControllerFilter implements Filter {
    public static final String COOKIE_NAME = "prv-sso-cookie";
    public static final String PROD_SITE = "https://pranveraapp.com";

    AuthenticationManager authenticationManager;

    private String hostAddress;

    public FrontControllerFilter(AuthenticationManager authenticationManager, String hostAddress){
        this.authenticationManager = authenticationManager;
        this.hostAddress = hostAddress;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        Cookie[] cookies = httpServletRequest.getCookies();

        Cookie cookieSSO = null;

        if(cookies != null)
            cookieSSO = Arrays.stream(cookies)
                .filter(c -> c != null && c.getName().equals(COOKIE_NAME))
                .peek(c -> System.out.println(c.getName()))
                .findAny()
                .orElse(null);


        // if cookie doesn't exist then go to prv-app-login
        if(cookieSSO == null && httpServletRequest.getParameter("token") == null){
            httpServletResponse.sendRedirect(hostAddress);
            log.info("Redirected to prv-app-login application for authentication");
            return;
        }
        // this should run only once, right after the prv-app-login success call
        else if( httpServletRequest.getParameter("token") != null
                && !httpServletRequest.getParameter("token").isEmpty()){
            String token = httpServletRequest.getParameter("token");
            String[] parts = token.split("\\.");
            String payload = null;
            try {
                payload = decode(parts[1]);
            } catch (DecoderException e) {
                e.printStackTrace();
            }
            String[] credentials = payload.trim().split("\\.");

            addCookieOnResponse(httpServletRequest, httpServletResponse);
            addAuthenticationInTheSession(httpServletRequest, authenticateUser(credentials[0], credentials[1]));
            httpServletRequest.getRequestDispatcher("/swagger-ui.html").forward(request ,response);
            return;
        }
        log.info("I have the session");
        filterChain.doFilter(request, response);
    }


    private static String decode(String encodedString) throws UnsupportedEncodingException, DecoderException {
        byte[] bytes = (new Base64()).decode(encodedString.getBytes());
        return new String(bytes);
    }

    private void addAuthenticationInTheSession(HttpServletRequest httpServletRequest, Authentication authentication) {
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(authentication);
        HttpSession session = httpServletRequest.getSession(true);
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
    }

    private Authentication authenticateUser(String username, String password) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        Authentication authentication = new UsernamePasswordAuthenticationToken(username, password, authorities);
        return authenticationManager.authenticate(authentication);
    }

    private void addCookieOnResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        Cookie cookie = new Cookie("prv-sso-cookie", httpServletRequest.getParameter("token"));
        cookie.setMaxAge(Integer.MAX_VALUE);
        httpServletResponse.addCookie(cookie);
    }

}
