package prv.inv.api.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.net.URL;
import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
public class MvcConfig extends WebMvcConfigurationSupport {


    @Value("${root.classpath}")
    String rootClasspath;


    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations(getPathOfstaticFolder());
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/css-error/**")
                .addResourceLocations("classpath:static/css-error/");
    }

    @Override
    protected void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        registry.viewResolver(resolver);
    }

    private String getPathOfstaticFolder(){
        ClassLoader classLoader = MvcConfig.class.getClassLoader();
        URL url = classLoader.getResource("static");
        String absolutePath = url.getPath();
        String root = absolutePath.substring(0,absolutePath.indexOf("prv-inv-api"));
        String relativePath = "file:".concat(root).concat(rootClasspath);

        log.info("Resources path: " , relativePath);
        return relativePath;
    }
}
