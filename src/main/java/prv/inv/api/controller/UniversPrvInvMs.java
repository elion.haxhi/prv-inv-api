package prv.inv.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import prv.inv.api.model.PersonListResource;
import prv.inv.api.model.UniversListResource;
import prv.inv.api.model.UniversReq;
import prv.inv.api.model.UniversResource;
import prv.inv.api.service.UniversService;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;

import static prv.inv.api.util.PrvInvConstants.TOKEN_JWT;

@RestController
@RequestMapping(value = "/univers")
@Api(value = "univers", tags = "Universe")
public class UniversPrvInvMs {

    private UniversService universService;

    public UniversPrvInvMs(UniversService universService){
        this.universService = universService;
    }

    @GetMapping(path = "query")
    @ApiOperation(value = "Trova tutti gli universi", notes = "Ritorna un UniversListResource")
    public UniversListResource queryUniverses(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @ApiParam(value = "La lista di attributti su cui cercare. " +
                              "Il parametro query più precisamente creca su stato e route_phase " +
                              "Es .../?query='STARTED VVV'", required = true)
            @RequestParam String query) throws NoSuchAlgorithmException{

        return universService.queryUniverses(query, authorization);

    }

    @GetMapping(path = "/")
    @ApiOperation(value = "Trova la lista dei universi filtrando per gli argomenti." +
                          "Possiamo filtrare con gli argomenti in modo separato.",
                  notes = "Ritorna un UniversListResource")
    public UniversListResource queryUniversesByParameters(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @ApiParam(value = "Lo stato del universo") @RequestParam(required = false) String status,
            @ApiParam(value = "La fase di route") @RequestParam(required = false) String routePhase,
            @ApiParam(value = "La data di creazione")
                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                    @RequestParam(required = false) LocalDate createdDate,
            @ApiParam(value = "Il peso del universo") @RequestParam(required = false) BigDecimal peso
            ) throws NoSuchAlgorithmException{
        return universService.queryUniversesByParameters(authorization,null, createdDate, peso, status, routePhase);
    }

    @GetMapping
    @ApiOperation(value = "Trova tutti gli universi che si trovano nel database", notes = "Ritorna un UniversListResource")
    public UniversListResource getUniverses(
            @ApiParam(value = "token")  @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization
            ) throws NoSuchAlgorithmException{
        return universService.getUniverses(authorization);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Trova l'universo filtrando per la variabile id",
                  notes = "Ritorna un UniversResource")
    public UniversResource getUniverseById(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @ApiParam(value = "L'id del universio da passare dome variabile", required = true)
            @PathVariable long id)throws NoSuchAlgorithmException {
        return universService.getUniverse(id, authorization);
    }

    @PostMapping
    @ApiOperation(value = "Crea un nuovo universo passando come body un oggetto Univers",
                  notes = "Ritorna un UniversResource")
    public UniversResource postUniverse(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @RequestBody UniversReq request) throws NoSuchAlgorithmException{
        return universService.createUnivers(authorization,request);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Aggiorna un Universo passando una variabile id e l'oggetto da aggiornare",
                  notes = "Ritorna un UniversResource")
    public UniversResource updateUniverseById(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @ApiParam(value = "L'id del universo da passare come variabile") @PathVariable long id,
            @ApiParam(value = "L'oggetto Univers da passare come body", required = true)
                  @RequestBody UniversReq universReq) throws NoSuchAlgorithmException{
        return universService.updateUnivers(authorization, universReq, id);
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Elimina un Universo passando come parametrolma variabile id",
                  notes = "Ritorna un UniversResource")
    public UniversResource deleteUniverseById(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @ApiParam(value = "L'id del universo da passare come variabile" , required = true)
            @PathVariable long id) throws NoSuchAlgorithmException{
        return universService.deleteUniverse(authorization, id);
    }

    @GetMapping(path = "{id}/person")
    @ApiOperation(value = "Trova tutte le persone che fanno", notes = "Ritorna un PersonResource")
    public PersonListResource getPersonsByUniversId(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @PathVariable long id) throws NoSuchAlgorithmException{
        return universService.getPersonListResourceByUniversId(authorization,id);
    }
}