package prv.inv.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import prv.inv.api.model.PersonResource;
import prv.inv.api.model.ThankYouResource;
import prv.inv.api.service.PersonService;

import java.security.NoSuchAlgorithmException;

import static prv.inv.api.util.PrvInvConstants.TOKEN_JWT;


@RestController
@RequestMapping(value = "/person")
@Api(value = "person", tags = "Person")
public class PersonPrvInvMs {

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Cerca una person per id", notes = "Ritorna un PersonResource")
    public PersonResource getPersonById(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization,
            @PathVariable long id) throws NoSuchAlgorithmException {

        return personService.retrieveAPersonResourceById(authorization, id);
    }
    @RequestMapping(value = "/thankyou", method = RequestMethod.GET)
    @ApiOperation(value = "Thanks", notes = "Ritorna un PersonResource")
    public ThankYouResource getGreeting(
            @RequestHeader(defaultValue = TOKEN_JWT, value = "Authorization") String authorization
           ) throws NoSuchAlgorithmException {

        return ThankYouResource.of("Thanks for watching!");
    }


}
