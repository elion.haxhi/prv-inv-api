package prv.inv.api.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

@Controller
//@RequestMapping(value = "prv-inv-api")
public class HomeController {


    public static final String INDEX = "index";

    @GetMapping(value = "/")
    public String index(HttpServletRequest request, HttpServletResponse response){
        return INDEX;
    }

    @GetMapping(value = "/logout2")
    public String logout(HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null)
            new SecurityContextLogoutHandler().logout(request, response, auth);
        // clear prv-sso-cookie
        clearCookie(request, response);
        // invalidate session
        request.getSession().invalidate();
        return "redirect:/";
    }

    private void clearCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie item = cookies[i];
                if ("prv-sso-cookie".equals(item.getName())) {
                    item.setMaxAge(0);
                    response.addCookie(item);
                }
            }
        }
    }


}
