package prv.inv.api.util;

public class PrvInvConstants {
    public static final String TOKEN_JWT = "e3R5cD1KV1QsIGFsZz1IUzI1Nn0." +
            "e3Bhc3N3b3JkPWFkbWluLCBpc3M9d3d3LmVsaW9uaGF4aGkuY29tLCB1c2VyPWVsaW9ufQ" +
            ".JsR-TlKyLSLl-fbZZPYQonP1cKZA7gR8wWY6e43zR3s";
    public static final String TOKEN_VALID_BUT_USER_NOT_EXIST ="Token credential are not present on db";
    public static final String OK_SAVE_CALL = "The call save is performed correctly";
    public static final String ERROR_SAVE_CALL = "The call save is performed with error";
    public static final String OK_UPDATE_CALL = "The call update is performed correctly";
    public static final String OK_CALL = "The call was executed correctly.";
    public static final String KO_CALL = "The call was executed with error.";
    public static final String NO_UNIVERS = "Non esiste un universo con tali dati.";

    private PrvInvConstants(){
        throw  new IllegalArgumentException("PrvInvConstants class");
    }

}
