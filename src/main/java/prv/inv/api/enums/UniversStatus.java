package prv.inv.api.enums;

public enum UniversStatus {

	PENDING,
	SCHEDULING,
	STARTED,
	ABORTED,
	CANCELED,
	ERROR,
	COMPLETED

}
