package prv.inv.api.exception;

public class UniversException extends RuntimeException{
    String message;

    public UniversException(String message) {
        super(message);
    }

}
