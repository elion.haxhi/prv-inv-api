package prv.inv.api.exception;

public class PayloadException extends RuntimeException{
    String message;

    public PayloadException(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }
}
